package de.tib.cli.owlapidr.jsonModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class ResourceClass {

	protected String iri = "";
	protected JSONObject obj = new JSONObject();
	protected Multimap<String, String> axioms = ArrayListMultimap.create();
	protected Multimap<String, String> annotations = ArrayListMultimap.create();
	protected String semanticType = "owl:Class";

	public ResourceClass() {
	}

	public ResourceClass(String iri) {
		this.iri = iri;
	}

	public String getIri() {
		return this.iri;
	}

	public void setSemanticType(String t) {
		// TODO: there could be multiple type assertions, 
		// for now we assume simply owl:Class 
		this.semanticType = t;
	}

	public void addAxiom(String axiom, String target) {
		if (!target.equals(iri)) {
			if (axioms.get(axiom) != null && axioms.get(axiom).contains(target)) {
				// do nothing,
				return;
			} else {
				axioms.put(axiom, target);
			}
		}
	}

	public void addAnnotation(String property, String value) {

		annotations.put(property, value);
	}

	public JSONObject getResult() {
		JSONObject axiomsObject = new JSONObject();
		JSONObject annotationObject = new JSONObject();
		// unroll the map
		for (Object key : this.axioms.keys()) {
			Collection<String> values = this.axioms.get(key.toString());
			axiomsObject.put(key.toString(), values);
		}
		for (Object key : this.annotations.keys()) {
			Collection<String> values = this.annotations.get(key.toString());
			annotationObject.put(key.toString(), values);
		}

		obj.put("identifier", this.iri);
		obj.put("type", this.semanticType);
		obj.put("axioms", axiomsObject);
		obj.put("annotations", annotationObject);
		return obj;
	}

}
