package de.tib.cli.owlapidr.jsonModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class RelationClass {
	protected String iri = "";
	protected String relationType = "";
	protected JSONObject obj = new JSONObject();
	protected Multimap<String, String> axioms = ArrayListMultimap.create();
	protected Multimap<String, String> annotations = ArrayListMultimap.create();
	protected ArrayList<String> domains = new ArrayList<String>();
	protected ArrayList<String> ranges = new ArrayList<String>();

	public RelationClass(String iri) {
		this.iri = iri;
	}

	public RelationClass(String iri, String propType) {
		this.iri = iri;
		this.relationType = propType;
	}

	public String getIri() {
		return this.iri;
	}

	public void setRelationType(String type) {
		this.relationType = type;
	}

	public void addDomain(String domain) {
		if (!this.domains.contains(domain)) {
			this.domains.add(domain);
		}
	}

	public void addRange(String range) {
		if (!this.ranges.contains(range)) {
			this.ranges.add(range);
		}
	}

	public String getRelationType() {
		return this.relationType;
	}

	public void addAxiom(String axiom, String target) {
		if (!target.equals(iri)) {
			if (axioms.get(axiom) != null && axioms.get(axiom).contains(target)) {
				// do nothing,
				return;
			} else {
				axioms.put(axiom, target);
			}
		}
	}

	public void addAnnotation(String property, String value) {

		if (annotations.get(property) != null && annotations.get(property).contains(value)) {
			// do nothing,
			return;
		} else {
			annotations.put(property, value);
		}
	}

	public JSONObject getResult() {

		// build domain range pairs;
		if (this.domains.size() == 1 && this.ranges.size() == 1) {
			// TODO this is the basic case, more complex ones!!!!
			List<Object> domainRangePairs = new ArrayList<>();
			JSONObject pair = new JSONObject();
			pair.put("domain", this.domains.get(0));
			pair.put("range", this.ranges.get(0));
			domainRangePairs.add(pair);
			obj.put("domainRangePairs", domainRangePairs);
		}

		JSONObject axiomsObject = new JSONObject();
		JSONObject annotationObject = new JSONObject();
		// unroll the map
		for (Object key : this.axioms.keys()) {
			Collection<String> values = this.axioms.get(key.toString());
			axiomsObject.put(key.toString(), values);
		}
		for (Object key : this.annotations.keys()) {
			Collection<String> values = this.annotations.get(key.toString());
			annotationObject.put(key.toString(), values);
		}

		obj.put("identifier", this.iri);
		obj.put("axioms", axiomsObject);
		obj.put("type", this.relationType);
		obj.put("annotations", annotationObject);
		return obj;
	}

}
