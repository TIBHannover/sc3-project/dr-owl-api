package de.tib.cli.owlapidr;

import org.apache.commons.cli.*;
import org.apache.commons.io.FilenameUtils;

import org.semanticweb.owlapi.model.IRI;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tib.cli.owlapidr.loader.*;

public class ConsoleMain {
	private static final String TASK_OPTION_NAME = "task";
	private static final String FILE_OPTION_NAME = "file";
	private static final String OUTPUT_OPTION_NAME = "output";
	private static final String DEPENDENCIES_OPTION_NAME = "dependencies";
	private static final String HELP_OPTION_NAME = "h";

	public static void main(String[] args) {

		new ConsoleMain().parseCommandLine(args);
	}

	protected void parseCommandLine(String[] args) {
		CommandLine line = null;
		Options options = createOptions();
		OntologyLoading loader = new OntologyLoading();
		loader.setReporter(new Reporter());

		try {
			line = new DefaultParser().parse(options, args);
		} catch (ParseException exp) {
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
			printHelpMenu(options);
			System.exit(0);
		}

		if (line.hasOption(HELP_OPTION_NAME)) {
			printHelpMenu(options);
			System.exit(0);
		}

		IRI ontologyIri;
		List<IRI> dependencies = new ArrayList<>();

		if (line.hasOption(FILE_OPTION_NAME)) {
			ontologyIri = IRI.create(new File(line.getOptionValue(FILE_OPTION_NAME)));
		} else {
			ontologyIri = IRI.create(new File(line.getOptionValue(FILE_OPTION_NAME)));
		}

		if (line.hasOption(DEPENDENCIES_OPTION_NAME)) {
			for (String path : line.getOptionValues(DEPENDENCIES_OPTION_NAME)) {
				File dependency = new File(path);
				dependencies.add(IRI.create(dependency));
			}
		}

		try {
			System.out.println("Input: " + line.getOptionValue(FILE_OPTION_NAME));
			System.out.println("Task: " + line.getOptionValue(TASK_OPTION_NAME));
			System.out.println("Output: " + line.getOptionValue(OUTPUT_OPTION_NAME));
			System.out.println("==================================================");
			loader.getReporter().setOutputName(line.getOptionValue(OUTPUT_OPTION_NAME));
//			******************************************************THIS IS WHERE WE PUT IN THE BRANCH
			if (line.getOptionValue(TASK_OPTION_NAME).compareTo("validate") == 0) {
				System.out.println("Starting validation procedure");
				loader.initializaeOntologyAndDependencies(ontologyIri, dependencies);
				loader.loadOntology();
				loader.validateOntology();
				loader.getReporter().writeReport();
			}

			if (line.getOptionValue(TASK_OPTION_NAME).compareTo("pre-init") == 0) {
				System.out.println("Starting pre-initialization procedure");
				loader.initializaeOntologyAndDependencies(ontologyIri, dependencies);
				loader.loadOntology();
				loader.validateOntology();
				loader.extractMetaInformation();
				System.out.println("==================================================");
				OntologyWrapper.writeOntologyAsTTL(ontologyIri, line.getOptionValue(FILE_OPTION_NAME),
						loader.getReporter());

				loader.getReporter().writeReport("preInit");
			}
			
			if (line.getOptionValue(TASK_OPTION_NAME).compareTo("jsonModel") == 0) {
				System.out.println("Creating JSON Model ");
				loader.initializaeOntologyAndDependencies(ontologyIri, dependencies);
				loader.loadOntology();
				loader.validateOntology();
				loader.extractMetaInformation();
				
				loader.createJSONModelAndWriteIt(line.getOptionValue(OUTPUT_OPTION_NAME));
				System.out.println("==================================================");
				
			}

			
			

		} catch (Exception e) {

			System.err.println("Something went wrong, see reporter output file for more details");
			if (line.getOptionValue(TASK_OPTION_NAME).compareTo("pre-init") == 0) {
				try {
					loader.getReporter().writeReport("preInit");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			if (line.getOptionValue(TASK_OPTION_NAME).compareTo("validate") == 0) {
				try {
					loader.getReporter().writeReport();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

//			// loader is now not scoped, means we can access it herel
//			String msg=loader.getLoadingInfoString();
//			System.err.println(msg);
//			
////			logger.error("FAILED TO LOAD " + Arrays.toString(args));
////			System.err.println(e.getClass().getName());
//			System.err.println(e.getMessage());

		}

	}

	protected Options createOptions() {
		Options options = new Options();
		options.addOption(Option.builder(HELP_OPTION_NAME).desc("views this help text").build());

		OptionGroup inputOptions = new OptionGroup();
		inputOptions.setRequired(true);

		inputOptions.addOption(Option.builder(FILE_OPTION_NAME).argName("PATH").hasArg()
				.desc("the local path to an ontology").build());
		options.addOption(Option.builder(DEPENDENCIES_OPTION_NAME).argName("PATHS").hasArgs()
				.desc("paths to dependencies of a local ontology").build());

		OptionGroup outputOptions = new OptionGroup();
		outputOptions.addOption(Option.builder(OUTPUT_OPTION_NAME).argName("PATH").hasArg()
				.desc("specify the path for the desired output location").build());

		OptionGroup taskOptions = new OptionGroup();
		taskOptions.setRequired(true);
		taskOptions.addOption(
				Option.builder(TASK_OPTION_NAME).argName("task").hasArg().desc("task specification for CLI, possible tasks: "
						+ "one of [validate, pre-init, jsonModel]\n"
						+ "validate: validates ontology and creates a json file with report\n"
						+ "pre-init: validates and extracts additional meta information\n"
						+ "jsonModel: returns the ResourceRelation model of the ontology").build());

		options.addOptionGroup(inputOptions);
		options.addOptionGroup(taskOptions);
		options.addOptionGroup(outputOptions);

		return options;
	}

	protected void printHelpMenu(Options options) {
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("java -jar owlapi-cli.jar", options);
	}

}
