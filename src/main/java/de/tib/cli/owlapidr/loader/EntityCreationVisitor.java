package de.tib.cli.owlapidr.loader;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectVisitor;

//import de.uni_stuttgart.vis.vowl.owl2vowl.model.entities.nodes.datatypes.VowlDatatype;
//import de.uni_stuttgart.vis.vowl.owl2vowl.model.entities.nodes.datatypes.VowlLiteral;
//import de.uni_stuttgart.vis.vowl.owl2vowl.model.entities.properties.VowlDatatypeProperty;
//import de.uni_stuttgart.vis.vowl.owl2vowl.model.entities.properties.VowlObjectProperty;

//
//import de.tib.cli.owlapidr.jsonModel.entities.nodes.classes.AbstractClass;
//import de.tib.cli.owlapidr.jsonModel.entities.nodes.classes.VowlClass;
//import de.tib.cli.owlapidr.jsonModel.entities.nodes.datatypes.VowlDatatype;
//import de.tib.cli.owlapidr.jsonModel.entities.nodes.datatypes.VowlLiteral;
//import de.tib.cli.owlapidr.jsonModel.entities.properties.VowlDatatypeProperty;
//import de.tib.cli.owlapidr.jsonModel.entities.properties.VowlObjectProperty;

public class EntityCreationVisitor implements OWLObjectVisitor {

	private DRJsonModel jsonModel;

	public EntityCreationVisitor(DRJsonModel jsonModel) {
		this.jsonModel = jsonModel;
	}

	@Override
	public void visit(OWLClass ce) {

		if (ce.isOWLThing() || ce.isOWLNothing()) {
			// General class do not create here
			return;
		} else if (!ce.isAnonymous()) {
			this.jsonModel.createResourceFromClass(ce);
		} else {
			return;
		}
	}

	public void visit(OWLLiteral node) {
		System.out.println("Visint OWL LITERAL NODE");
	}

	@Override
	public void visit(OWLDatatype node) {
		System.out.println("Visint OWL DATATYE NODE");
//		if (node.getIRI().toString().equals(VowlLiteral.LITERAL_IRI)) {
//			// Skip generic literal. Already included in the data as default.
//			return;
//		}
//
//		vowlData.addDatatype(new VowlDatatype(node.getIRI()));
	}

	@Override
	public void visit(OWLObjectProperty property) {
		
		this.jsonModel.createRelationFromObjectProperty(property);
		
//		VowlObjectProperty prop;
//
//		if (!property.isAnonymous()) {
//			prop = new VowlObjectProperty(property.getIRI());
//		} else {
//			// TODO anonymous behaviour
//			logger.info("Anonymous OWLObjectProperty " + property);
//			return;
//		}
//
//		vowlData.addObjectProperty(prop);
	}

	@Override
	public void visit(OWLDataProperty property) {
		System.out.println("Visiting DATATYPE PROPERTY");
		this.jsonModel.createRelationFromDatatypeProperty(property);
//		VowlDatatypeProperty prop;
//
//		if (!property.isAnonymous()) {
//			prop = new VowlDatatypeProperty(property.getIRI());
//		} else {
//			// TODO anonymous behaviour
//			logger.info("Anonymous OWLDataProperty " + property);
//			return;
//		}
//
//		vowlData.addDatatypeProperty(prop);
	}

}
