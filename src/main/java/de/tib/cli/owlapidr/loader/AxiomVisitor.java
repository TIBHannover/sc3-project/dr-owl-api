package de.tib.cli.owlapidr.loader;

import org.semanticweb.owlapi.model.OWLAxiomVisitor;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLPropertyDomain;

import de.tib.cli.owlapidr.jsonModel.RelationClass;

public class AxiomVisitor implements OWLAxiomVisitor {

	private DRJsonModel jsonModel;
	private RelationClass currentRelation;

	public AxiomVisitor(DRJsonModel jsonModel) {
		this.jsonModel = jsonModel;
	}

	public void setCurrentRelation(RelationClass rel) {
		this.currentRelation = rel;
	}

	@Override
	public void visit(OWLObjectPropertyDomainAxiom domainAxiom) {
		OWLClassExpression d = domainAxiom.getDomain();

		String asPrefixed = jsonModel.createShortFormForIri(d.asOWLClass().getIRI());
		this.currentRelation.addDomain(asPrefixed);

	}

	@Override
	public void visit(OWLObjectPropertyRangeAxiom rangeAxiom) {
		OWLClassExpression d = rangeAxiom.getRange();

		String asPrefixed = jsonModel.createShortFormForIri(d.asOWLClass().getIRI());
		this.currentRelation.addRange(asPrefixed);

	}

	@Override
	public void visit(OWLDataPropertyDomainAxiom domainAxiom) {
		OWLClassExpression d  = domainAxiom.getDomain();
		String asString=jsonModel.createShortFormForIri(d.asOWLClass().getIRI());
		this.currentRelation.addDomain(asString);

	}
	
	
	
	@Override
	public void visit(OWLDataPropertyRangeAxiom rangeAxiom) {
		OWLDataRange d = rangeAxiom.getRange();

		String asString=d.toString();
		this.currentRelation.addRange(asString);

	}
	

}
