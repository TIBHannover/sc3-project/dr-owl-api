package de.tib.cli.owlapidr.loader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.json.JSONObject;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationSubject;
import org.semanticweb.owlapi.model.OWLAnnotationValue;
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNegativeDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNegativeObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.model.OWLSubAnnotationPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.util.OWLOntologyWalker;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.tib.cli.owlapidr.loader.EntityCreationVisitor;
import de.tib.cli.owlapidr.jsonModel.RelationClass;
import de.tib.cli.owlapidr.jsonModel.ResourceClass;

public class DRJsonModel {
	protected OWLOntologyManager manager;
	protected OWLOntology ontology;

	protected String ontologyURI = "";
	PrefixDocumentFormat prefixManager;
	private BidiMap<String, String> prefixMap = new DualHashBidiMap<>();
	private Multimap<String, String> ontologyProperties = ArrayListMultimap.create();
	protected List<String> listOfImportOntologies = new ArrayList<>();

	private Map<String, ResourceClass> resourceMap = new HashMap<String, ResourceClass>();
	private Map<String, RelationClass> relationMap = new HashMap<String, RelationClass>();
	protected int counter = 1;
	protected int rel_counter = 1;

	public DRJsonModel(OWLOntologyManager manager, OWLOntology ontology) {
		// we have only one constructor , it takes the manager and the ontology
		// it is used to extract the ontology Id and the prefix map;
		this.manager = manager;
		this.ontology = ontology;

		// we should be able to set directly the ontology IRI;

		this.setOntologyURI(ontology.getOntologyID().getOntologyIRI().get().toString());
		this.setPrefixListFromManager(manager);

//		test();

		// create the list of ontology Properties;
		// the axioms assigned by the definition of the ontology node itself;
		this.createOntologyProperties();
		this.createResources();

	}

	public void setOntologyURI(String URI) {
		this.ontologyURI = URI;
	}

	public void setPrefixListFromManager(OWLOntologyManager manager) {
		this.prefixManager = (PrefixDocumentFormat) manager.getOntologyFormat(ontology);
		// lets extract the prefix map
		Map<String, String> prefixName2PrefixMap = this.prefixManager.getPrefixName2PrefixMap();
		// adding prefix list to that thing;
		for (Map.Entry<String, String> entry : prefixName2PrefixMap.entrySet()) {
			this.addPrefix(entry.getKey(), entry.getValue());
		}

	}

	public void addPrefix(String prefix, String iri) {
		this.prefixMap.put(prefix, iri);
	}

	public void createOntologyProperties() {
		Boolean isOntology = ontology.isOntology();
		if (isOntology) {
			OntologyAnnotations oA = new OntologyAnnotations(manager, ontology);
			Stream<IRI> imports = ontology.directImportsDocuments();
			imports.forEach(x -> this.addImportOntology(x.toString()));
			// ^^^^^^^^^^ TOD CREATE BETTER VALIDATION

			Stream<OWLAnnotation> annotations = ontology.annotations();
			annotations.forEach(item -> {

				OWLAnnotationProperty prop = item.getProperty();
				String propAsString = prop.getIRI().toString();
				String pref = prop.getIRI().getNamespace();
				String suffix = prop.getIRI().getShortForm();

				String propDef = this.createShortForm(pref, suffix);

				if (!item.getValue().isIRI()) {
					OWLLiteral val = item.getValue().asLiteral().get();
					String valueAsString = val.toString();
					ontologyProperties.put(propDef, valueAsString);
				} else {
					String valueAsString = item.getValue().asIRI().get().toString();
					ontologyProperties.put(propDef, valueAsString);
				}
			});

		}

	}

	public String createShortFormForIri(IRI iri) {
		return createShortForm(iri.getNamespace(), iri.getShortForm());
	}

	public String createShortForm(String prefix, String suffix) {
		// look up the key where the prefix is the value;
		if (this.prefixMap.containsValue(prefix)) {

			String prefixValue = this.prefixMap.getKey(prefix);
			return prefixValue + suffix;

		}
		return prefix + suffix;

	}

	public void createResourceFromClass(OWLClass input) {
		// we assume it is a class
		// lets try to see if we can recreate a body of that class;

		// try to find it in the map
		// TODO make it short
		String classIri = this.createShortFormForIri(input.getIRI());
		ResourceClass cl;
		if (!resourceMap.containsKey(classIri)) {
			cl = new ResourceClass(classIri);
			resourceMap.put(classIri, cl);
			System.out.println(counter + "--Adding Class  " + classIri);
			counter += 1;
			// todo populate the data;

		} else {
			cl = resourceMap.get(classIri);
		}

		if (counter == 75) {
			int breaker = 23;
		}

//
//		// process axioms;
		Stream<OWLClassAxiom> axioms = ontology.axioms(input);
		int wait = 23;
		System.out.println("number of axioms" + axioms.count());
		axioms = ontology.axioms(input);
		axioms.forEach(itemAxiom -> {
			System.out.println(itemAxiom.toString());
			AxiomType<?> ax = itemAxiom.getAxiomType();
			String axiomName = ax.getName();
			System.out.println(axiomName);
			System.out.println("----------------");
			/** Declaration. */
			/** [x] EquivalentClasses. */
			/** [x] SubClassOf. */
			/** DisjointClasses. */
			/** DisjointUnion. */
			/** ClassAssertion. */
			/** SameIndividual. */
			/** DifferentIndividuals. */
			/** ObjectPropertyAssertion. */
			/** NegativeObjectPropertyAssertion. */
			/** DataPropertyAssertion. */
			/** NegativeDataPropertyAssertion. */
			/** EquivalentObjectProperties. */
			/** SubObjectPropertyOf. */
			/** InverseObjectProperties. */
			/** FunctionalObjectProperty. */
			/** InverseFunctionalObjectProperty. */
			/** SymmetricObjectProperty. */
			/** AsymmetricObjectProperty. */
			/** TransitiveObjectProperty. */
			/** ReflexiveObjectProperty. */
			/** IrreflexiveObjectProperty. */
			/** ObjectPropertyDomain. */
			/** ObjectPropertyRange. */
			/** DisjointObjectProperties. */
			/** SubPropertyChainOf. */
			/** EquivalentDataProperties. */
			/** SubDataPropertyOf. */
			/** FunctionalDataProperty. */
			/** DataPropertyDomain. */
			/** DataPropertyRange. */
			/** DisjointDataProperties. */
			/** DatatypeDefinition. */
			/** HasKey. */
			/** Rule. */
			/** AnnotationAssertion. */
			/** SubAnnotationPropertyOf. */
			/** AnnotationPropertyRangeOf. */
			/** AnnotationPropertyDomain. */
			switch (axiomName) {
			case "EquivalentClasses": {
				OWLEquivalentClassesAxiom thatAx = (OWLEquivalentClassesAxiom) itemAxiom;
				Stream<OWLClass> classes = thatAx.namedClasses();
				classes.forEach(classes_item -> {
					cl.addAxiom("owl:equivalentClass", this.createShortFormForIri(classes_item.getIRI()));
				});
			}
				break;
			case "SubClassOf": {
				OWLSubClassOfAxiom thatAx = (OWLSubClassOfAxiom) itemAxiom;
				try {
					OWLClass exp = thatAx.getSuperClass().asOWLClass();
					cl.addAxiom("rdfs:subClassOf", this.createShortFormForIri(exp.getIRI()));
				} catch (Exception e) {
					OWLClassExpression expression = thatAx.getSuperClass();
					System.out.println(expression.getClass().toString());
					System.out.println("Super Class is a blank node (possible restriction)"
							+ expression.getClassExpressionType().toString());

					int breaker = 23;

				}

			}
				break;

			default:
				break;
			}

		});
//		cl.seeResults();
	}

	public void createResources() {
		System.out.println("creating resources array");
		// this is a set of annotation assertions;
		OWLOntologyWalker walker = new OWLOntologyWalker(ontology.importsClosure().collect(Collectors.toSet()));
		EntityCreationVisitor ecv = new EntityCreationVisitor(this);

		try {
			walker.walkStructure(ecv);

		} catch (Exception e) {
			System.out.println("Walking " + e.getMessage());
		}

		Set<OWLAxiom> ax = ontology.axioms(Imports.INCLUDED).collect(Collectors.toSet());
		for (OWLAxiom entry : ax) {
			try {
				// try to cast this as assertion axiom and add it to VOWL Data
				OWLAnnotationAssertionAxiom castedEntry = (OWLAnnotationAssertionAxiom) entry;
				OWLAnnotationSubject oas = castedEntry.getSubject();
				IRI subjectIRI = castedEntry.getSubject().asIRI().get();
				String subject = this.createShortFormForIri(subjectIRI);
				IRI propertyIRI = castedEntry.getProperty().getIRI();
				String property = this.createShortFormForIri(propertyIRI);
				String value = "";

				OWLAnnotationValue le_val = castedEntry.getValue();

//				OWLLiteral le_literal = le_val.asLiteral().get();
				value = le_val.toString();
				// TODO CURRENTLY WE LOSE THE LANGUAGE TAG -.-
				// try to finde the iri in the map
				try {
					ResourceClass cl = this.resourceMap.get(subject);
					cl.addAnnotation(property, value);
				} catch (Exception e) {
					// this is not a class;
					System.out.println(subject + " is not a Class.");
					try {
						RelationClass rel = this.relationMap.get(subject);
						rel.addAnnotation(property, value);
					} catch (Exception e2) {
						System.out.println(subject + " is not a Relation.");
					}
				}

			} catch (Exception e) {
				// we dont care
				// this is a declaration axiom and not an assertion axiom , we are interessted
				// in assertion to baseConstructors
			}
		}

		System.out.println("creating resources array");
	}

	public void addImportOntology(String importedURI) {
		listOfImportOntologies.add(importedURI);
	}

	public void createRelationFromObjectProperty(OWLObjectProperty prop) {
		AxiomVisitor av = new AxiomVisitor(this);
		String propType = "owl:ObjectProperty";
		String propIri = this.createShortFormForIri(prop.getIRI());

		RelationClass rel;
		if (!relationMap.containsKey(propIri)) {
			rel = new RelationClass(propIri, propType);
			relationMap.put(propIri, rel);
			System.out.println(rel_counter + "--Adding Relation  " + propIri);
			// todo populate the data;

		} else {
			rel = relationMap.get(propIri);
		}

		Stream<OWLObjectPropertyAxiom> axioms = ontology.axioms(prop);
		int wait = 23;
		System.out.println("number of axioms" + axioms.count());
		axioms = ontology.axioms(prop);
		av.setCurrentRelation(rel);
		axioms.forEach(itemAxiom -> {
			System.out.println(itemAxiom.toString());
			AxiomType<?> ax = itemAxiom.getAxiomType();
			String axiomName = ax.getName();
			System.out.println(axiomName);
			System.out.println("----------------");

			itemAxiom.accept(av);

		});

	}

	public void createRelationFromDatatypeProperty(OWLDataProperty prop) {
		AxiomVisitor av = new AxiomVisitor(this);
		String propType = "owl:DatatypeProperty";
		String propIri = this.createShortFormForIri(prop.getIRI());

		RelationClass rel;
		if (!relationMap.containsKey(propIri)) {
			rel = new RelationClass(propIri, propType);
			relationMap.put(propIri, rel);
			System.out.println(rel_counter + "--Adding Relation  " + propIri);
			// todo populate the data;

		} else {
			rel = relationMap.get(propIri);
		}

		Stream<OWLDataPropertyAxiom> axioms = ontology.axioms(prop);
		int wait = 23;
		System.out.println("number of axioms" + axioms.count());
		axioms = ontology.axioms(prop);
		av.setCurrentRelation(rel);
		axioms.forEach(itemAxiom -> {
			System.out.println(itemAxiom.toString());
			AxiomType<?> ax = itemAxiom.getAxiomType();
			String axiomName = ax.getName();
			System.out.println(axiomName);
			System.out.println("----------------");

			itemAxiom.accept(av);

		});

	}

	public void showModel() {

//		obj.put("ontology", this.ontologyURI);
//		obj.put("prefixMap", this.prefixMap);
//		obj.put("ontologyProperties", this.ontologyProperties);

		JSONObject obj = getModel();
		System.out.println(obj.toString());
		System.out.println("Do we have the data? ");

	}

	public JSONObject getModel() {
		JSONObject obj = new JSONObject();
		ResourceClass[] array = this.resourceMap.values().toArray(new ResourceClass[0]);
		List<Object> resources = new ArrayList<>();
		for (ResourceClass item : array) {
			System.out.println(item.getResult().toString());
			resources.add(item.getResult());
		}

		RelationClass[] rel_array = this.relationMap.values().toArray(new RelationClass[0]);
		List<Object> relations = new ArrayList<>();
		for (RelationClass item : rel_array) {
			System.out.println(item.getResult().toString());
			relations.add(item.getResult());
		}

		obj.put("resources", resources);
		obj.put("relations", relations);

		return obj;
	}

	public void writeToFile(String fileName) {
		JSONObject jsModel = getModel();
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(jsModel.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

// * data model definition 

//1 json object, duuuhh
/*
 * 
 * { ontology: "URI", prefixMap: { list of prefix}, ontologyProperties:{ list of
 * assigned properties to this ontology} ontoloogyStatistics{ statistics fetched
 * from arq} imports:{ list of used imports and their status} resources:[ array
 * of resources] relations: [ array of relations] // annotation properties are
 * relations }
 */
