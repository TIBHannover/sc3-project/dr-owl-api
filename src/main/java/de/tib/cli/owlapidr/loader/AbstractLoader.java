package de.tib.cli.owlapidr.loader;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationValue;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;

import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.OWLOntologyWalker;

import java.util.Collection;
import java.util.Set;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Abstract converter which processes the most part of the converting. The sub
 * classes can specify the source of the ontology or to some additional
 * processing if necessary.
 */
public abstract class AbstractLoader implements Loader {

	//private static final Logger logger = LogManager.getLogger(AbstractLoader.class);
	
	protected String loadedOntologyPath;
	protected String loadingInfoString = "";
	protected boolean missingImports = false;
	protected OWLOntologyManager manager;
	protected OWLOntology ontology;
	protected boolean initialized = false;
	protected OWLAPI_MissingImportsListener missingListener = null;
	protected boolean currentlyLoading = false;
	protected String parentLine = "";
	protected Reporter reporter;

	public void setCurrentlyLoadingFlag(boolean val) {
		this.parentLine = "";
		currentlyLoading = val;
	}
	
	public Reporter getReporter() {
		return reporter;												
	}
	
	public void setReporter(Reporter r) {
		reporter=r;
	}
	
	public void releaseMemory() {
		manager.removeMissingImportListener(missingListener);
		manager.removeOntology(ontology);
		missingListener = null;
		manager = null;
		ontology = null;
	}

	public void setCurrentlyLoadingFlag(String parentLine, boolean val) {
		// Line where to append to
		this.parentLine = parentLine;
		currentlyLoading = val;
	}

	public boolean getCurrentlyLoadingFlag() {
		return currentlyLoading;
	}

	public void clearLoadingMsg() {
		loadingInfoString = "";
	}

	public String msgForWebVOWL(String stackTrace) {
		// converts the < and > tags to html string so no html injection is created
		String s1 = stackTrace.replaceAll("<", "&lt;");
		String s2 = s1.replaceAll(">", "&gt;");
		return s2;
	}

	public boolean ontologyHasMissingImports() {
		return missingImports;
	}

	public void setOntologyHasMissingImports(boolean val) {
		missingImports = val;
	}

	public String getLoadingInfoString() {
		return loadingInfoString;
	}

	public void addLoadingInfo(String msg) {
		loadingInfoString += msg;
	}

	public void addLoadingInfoToLine(String parent, String msg) {
		// find parent line in msg;
		if (loadingInfoString.length() > 0) {
			String tokens[] = loadingInfoString.split("\n");
			for (int i = 0; i < tokens.length; i++) {
				if (tokens[i].contains(parent)) {
					tokens[i] += msg;
				}
			}
			// replace loading msg;
			loadingInfoString = "";
			for (int i = 0; i < tokens.length - 1; i++) {
				if (tokens[i].length() > 0) {
					loadingInfoString += tokens[i] + "\n";
				}
			}
			loadingInfoString += tokens[tokens.length - 1] + "\n";
		}
	};

	public void addLoadingInfoToParentLine(String msg) {
		if (this.parentLine.length() > 0) {
			addLoadingInfoToLine(this.parentLine, msg);
		}
	}

	private void preLoadOntology() throws Exception {
		try {
			loadOntology();
		} catch (OWLOntologyCreationException e) {
			throw new RuntimeException(e);
		}
		initialized = true;
	}

	protected abstract void loadOntology() throws Exception; 
	{	
	}
	
	protected abstract void loadOntologyWithoutImports() throws Exception; 
	{	
	}


	private void parsing(OWLOntology ontology, OWLOntologyManager manager) {
		
	}

	private void processIndividuals(OWLOntology ontology, OWLOntologyManager manager) {
			}

	private void processObjectProperties(OWLOntology ontology) {
			}

	private void processDataProperties(OWLOntology ontology) {
		
	}

}
