package de.tib.cli.owlapidr.loader;

import java.util.HashMap;
import java.util.Map;

import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class OntologyAnnotations {

	public static final String RDFS_COMMENT = "rdfs:comment";
	public static final String RDFS_LABEL = "rdfs:label";
	public static final String RDFS_DEFINED_BY = "rdfs:isDefinedBy";
	public static final String OWL_VERSIONINFO = "owl:versionInfo";
	public static final String RDFS_SUBCLASS = "rdfs:subClassOf";
	public static final String RDFS_DOMAIN = "rdfs:domain";
	public static final String RDFS_RANGE = "rdfs:range";
	public static final String DC_DESCRIPTION = "dc:description";
	public static final String DC_TITLE = "dc:title";
	public static final String TERMS_CREATED = "terms:created";
	public static final String TERMS_CREATOR = "terms:creator";
	public static final String TERMS_IS_VERSION_OF = "terms:isVersionOf";
	public static final String TERMS_MODIFIED = "terms:modified";
	public static final String TERMS_TITLE = "terms:title";
	public static final String VANN_PREFF_NAMESPACE = "vann:preferredNamespaceUri";

	private Map<String, String> prefixMap = new HashMap<>();

	public OntologyAnnotations(OWLOntologyManager manager, OWLOntology ontology) {
		PrefixDocumentFormat pm = (PrefixDocumentFormat) manager.getOntologyFormat(ontology);
		Map<String, String> prefixName2PrefixMap = pm.getPrefixName2PrefixMap();
		// adding prefix list to that thing;
		for (Map.Entry<String, String> entry : prefixName2PrefixMap.entrySet()) {
			this.addPrefix(entry.getKey(), entry.getValue());
		}

	}

	protected void addPrefix(String prefix, String iri) {
		prefixMap.put(prefix, iri);
	}

	public String asIRI(String input) {
		// split input by :
		String[] tokens = input.split(":");
		String prefixLookup = tokens[0] + ":";
		String longPrefix = this.prefixMap.get(prefixLookup);
		if (longPrefix.length() > 0) {
			return longPrefix + tokens[1];
		}

		return input;
	}

}
