package de.tib.cli.owlapidr.loader;

import org.json.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reporter {

	protected List<String> parserMessages = new ArrayList<>();
	protected List<String> importMessages = new ArrayList<>();
	protected List<String> ignoredImports = new ArrayList<>();
	protected List<String> validationMessages = new ArrayList<>();
	protected List<String> listOfImportOntologies = new ArrayList<>();
	protected String preInit_description = "";
	protected String preInit_title = "";
	protected String outputFileName = "output.txt";

	public Reporter() {
	}

	public void addParserMessage(String msg) {
		parserMessages.add(msg);
	}

	public void setOutputName(String name) {
		if (name != null) {
			outputFileName = name;
		}
	}

	public void addImportMessage(String msg) {
		importMessages.add(msg);
	}
	public void addIgnoreImport(String msg) {
		ignoredImports.add(msg);
	}

	public void addImportOntology(String msg) {
		listOfImportOntologies.add(msg);
	}

	public void addValidationMessage(String msg) {
		validationMessages.add(msg);
	}

	public void setPreInitDescription(String description) {
		this.preInit_description = description;
	}

	public void setPreInitTitle(String title) {
		this.preInit_title = title;
	}

	public void writeReport() throws IOException {

		// we use a local file;
		// 1) parser report
		String ontologyReportFileName = this.outputFileName;

		// it will be written as a JSON Object.
		JSONObject obj = new JSONObject();

		// create parser messages
		if (parserMessages.isEmpty()) {
			obj.put("parser", "successful");

			// proceeding with other messages only when parser was successful;
			// create import messages
			if (importMessages.isEmpty()) {
				obj.put("imports", "successful");
			} else {
				obj.put("imports", "failed");
				obj.put("importErrors", importMessages);
			}

			// create validation messages
			if (validationMessages.isEmpty()) {
				obj.put("validation", "successful");
			} else {
				obj.put("validation", "failed");
				obj.put("validationErrors", validationMessages);
			}

		} else {
			obj.put("parser", "failed");
			obj.put("parserError", parserMessages);
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(ontologyReportFileName));
		writer.write(obj.toString());
		writer.close();

	}

	public void writeReport(String task) throws IOException {

		if (task.equals("preInit")) {
			String ontologyReportFileName = this.outputFileName;
			// it will be written as a JSON Object.
			JSONObject obj = new JSONObject();

			// create parser messages
			if (parserMessages.isEmpty()) {
				obj.put("parser", "successful");

				// proceeding with other messages only when parser was succesfull;
				// create import messages
				if (importMessages.isEmpty()) {
					obj.put("imports", "successful");
				} else {
					obj.put("imports", "failed");
					obj.put("importErrors", importMessages);
				}

				// create import ontologies
				if (!listOfImportOntologies.isEmpty()) {
					obj.put("importedOntologies", listOfImportOntologies);
				}

				// create parser messages
				if (validationMessages.isEmpty()) {
					obj.put("validation", "successful");
				} else {
					obj.put("validation", "failed");
					obj.put("validationErrors", validationMessages);
				}

			} else {
				obj.put("parser", "failed");
				obj.put("parserError", parserMessages);
			}

			obj.put("description", this.preInit_description);
			obj.put("title", this.preInit_title);

			BufferedWriter writer = new BufferedWriter(new FileWriter(ontologyReportFileName));
			writer.write(obj.toString());
			writer.close();

		}
	}

}
