package de.tib.cli.owlapidr.loader;

//import de.tib.cli.owlapidr.loader.AbstractLoader;

import java.util.HashMap;
import java.util.Map;
import org.semanticweb.owlapi.model.MissingImportEvent;
import org.semanticweb.owlapi.model.MissingImportListener;


@SuppressWarnings("serial")
public class OWLAPI_MissingImportsListener implements MissingImportListener {
	Map<String, Boolean> missingImportsMap;
	AbstractLoader currentConverter;
	public OWLAPI_MissingImportsListener(AbstractLoader c) {
		 currentConverter=c;
		 missingImportsMap=new HashMap<String, Boolean>();
	};

	@Override
	public void importMissing(MissingImportEvent event) {
		String missingName=event.getImportedOntologyURI().toString();
		currentConverter.getReporter().addImportMessage("Failed to import <"+missingName+">");
		currentConverter.getReporter().addIgnoreImport(missingName);
		missingImportsMap.put(event.getImportedOntologyURI().toString(),false);
		currentConverter.setOntologyHasMissingImports(true);
	}
	
	public Map<String, Boolean> getMissingImportsMap(){
		return missingImportsMap;
	}
}

