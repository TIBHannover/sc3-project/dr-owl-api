package de.tib.cli.owlapidr.loader;


public interface Loader {
	String getLoadingInfoString();
	void setOntologyHasMissingImports(boolean val);
	boolean ontologyHasMissingImports();
	void addLoadingInfo(String msg);
	boolean getCurrentlyLoadingFlag();
	void setCurrentlyLoadingFlag(boolean val);
	void setCurrentlyLoadingFlag(String parentLine,boolean val);
	void addLoadingInfoToParentLine(String msg);
	void createJSONModel();
	
}
