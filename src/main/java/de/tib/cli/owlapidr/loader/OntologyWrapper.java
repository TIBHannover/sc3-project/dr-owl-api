package de.tib.cli.owlapidr.loader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.eclipse.rdf4j.rio.turtle.TurtleParser;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.formats.RioTurtleDocumentFormat;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.OWLParserException;
import org.semanticweb.owlapi.io.UnparsableOntologyException;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.MissingImportHandlingStrategy;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class OntologyWrapper {

	public static void writeOntologyAsTTL(IRI mainOntology, String inputName, Reporter r) {
		OWLOntologyManager manager= OWLManager.createOWLOntologyManager();
		OWLOntology ontology;
		manager.getOntologyConfigurator().setMissingImportHandlingStrategy(MissingImportHandlingStrategy.SILENT);
		
		try {
			 List<String> ignoredImports = r.ignoredImports;
			
			 for (int i=0;i<ignoredImports.size();i++)
				 manager.getOntologyConfigurator().addIgnoredImport(IRI.create(ignoredImports.get(i)));	
			
			ontology = manager.loadOntology(mainOntology);

			File testFile = new File(inputName + ".ttl");
			TurtleDocumentFormat ttlParser= new TurtleDocumentFormat(); 
			PrefixDocumentFormat pm = (PrefixDocumentFormat) manager.getOntologyFormat(ontology);
			ttlParser.copyPrefixesFrom(pm);
			
			OutputStream outputStream = new FileOutputStream(testFile);
			manager.saveOntology(ontology, ttlParser, outputStream);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
