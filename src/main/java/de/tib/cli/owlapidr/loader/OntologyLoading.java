package de.tib.cli.owlapidr.loader;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.MissingImportHandlingStrategy;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.io.UnparsableOntologyException;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.OWLParserException;

import de.tib.cli.owlapidr.loader.Validator;

import java.util.Collection;
import java.util.Collections;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.ArrayList;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.BufferedWriter;
import java.io.File; // Import the File class
import java.io.FileOutputStream;
import java.io.FileWriter;

import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.json.JSONObject;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;

public class OntologyLoading extends AbstractLoader {
	protected IRI mainOntology;
	protected Collection<IRI> depdencyOntologies;
	// private static final Logger logger =
	// LogManager.getLogger(OntologyLoading.class.getName());

	public OntologyLoading() {
	}

	public OntologyLoading(IRI ontologyIRI) {
		this(ontologyIRI, Collections.<IRI>emptyList());
	}

	public OntologyLoading(IRI ontologyIRI, Collection<IRI> necessaryExternals) {
		mainOntology = ontologyIRI;
		depdencyOntologies = Collections.unmodifiableCollection(necessaryExternals);
	}

	public void initializaeOntologyAndDependencies(IRI ontologyIRI, Collection<IRI> necessaryExternals) {
		mainOntology = ontologyIRI;
		depdencyOntologies = Collections.unmodifiableCollection(necessaryExternals);
	}

	public void extractMetaInformation() {
		System.out.println("    Extracting Meta Information==============================================");

		Boolean isOntology = ontology.isOntology();
		if (isOntology) {
//		Set<Annotation> ontologyAnnotations = new AnnotationVisitor(vowlData, null).getOntologyAnnotations(ontology);

			OntologyAnnotations oA = new OntologyAnnotations(manager, ontology);

			System.out.println("Okay it is an ontolology");
			OWLDocumentFormat format = ontology.getFormat();
			System.out.println("Whats the format? " + format.toString());
			Stream<IRI> imports = ontology.directImportsDocuments();
			imports.forEach(x -> reporter.addImportOntology(x.toString()));

			Stream<OWLAnnotation> annotations = ontology.annotations();
			annotations.forEach(item -> {
				OWLAnnotationProperty prop = item.getProperty();
				String propAsString = prop.getIRI().toString();
				if (propAsString.equals(oA.asIRI(OntologyAnnotations.DC_DESCRIPTION))) {
					OWLLiteral val = item.getValue().asLiteral().get();
					// currently enforced English language tag
					// ** TODO ** fix method to determine the correct item to export.
					if (val.hasLang() && val.hasLang("en")) {
						reporter.setPreInitDescription(val.getLiteral());
					}
				}
				if (propAsString.equals(oA.asIRI(OntologyAnnotations.DC_TITLE))) {
					OWLLiteral val = item.getValue().asLiteral().get();
					// currently enforced English language tag
					// ** TODO ** fix method to determine the correct item to export.
					if (val.hasLang() && val.hasLang("en")) {
						reporter.setPreInitTitle(val.getLiteral());
					}
				}
			});

//			Set<OWLAxiom> testAxioms = ontology.getABoxAxioms(null);
//			
//			Stream<OWLAxiom> axioms = ontology.aboxAxioms(null);

		}

	}

	public void validateOntology() {
		System.out.println("    Validating==============================================");
		Validator v = new Validator();
		String result = v.generateFullReport(ontology);
		if (result.length() != 0) {
			reporter.addValidationMessage(result);
			System.out.println(result);
		}

	}

	public void loadOntologyWithoutImports() throws Exception {

	}

	@Override
	public void loadOntology() throws Exception {
		// logger.info("Converting IRIs...");

		// logger.info("Loading ontologies ... [" + mainOntology + ", " +
		// depdencyOntologies + "]");
		this.setOntologyHasMissingImports(false);
		manager = OWLManager.createOWLOntologyManager();

		if (!depdencyOntologies.isEmpty()) {
			for (IRI externalIRI : depdencyOntologies) {
				manager.loadOntology(externalIRI);
			}
			// logger.info("External ontologies loaded!");
		}

		try {
			this.addLoadingInfo("* Parsing ontology with OWL API ");
			this.setCurrentlyLoadingFlag("* Parsing ontology with OWL API ", true);
			missingListener = new OWLAPI_MissingImportsListener(this);
			manager.addMissingImportListener(missingListener);
			manager.getOntologyConfigurator().setMissingImportHandlingStrategy(MissingImportHandlingStrategy.SILENT);
			manager.getOntologyConfigurator().setFollowRedirects(true);
			manager.getOntologyConfigurator().setConnectionTimeout(100000);

			manager.getOntologyConfigurator().setAcceptingHTTPCompression(true);
			manager.getOntologyConfigurator().withRepairIllegalPunnings(false);

			ontology = manager.loadOntology(mainOntology);

			this.addLoadingInfoToParentLine("... done ");
			this.setCurrentlyLoadingFlag(false);
			if (this.ontologyHasMissingImports() == true) {
				this.addLoadingInfoToLine("* Parsing ontology with OWL API ",
						"<span style='color:yellow;'>(with warnings)</span>");
			}

		} catch (UnparsableOntologyException e) {
			this.addLoadingInfoToLine("* Parsing ontology with OWL API ", "... <span style='color:red;'>failed</span>");
			this.setCurrentlyLoadingFlag(false);

			// hackish, get only the ttl parser err and adds this to the loading info string
			Set<Map.Entry<OWLParser, OWLParserException>> mapSet = e.getExceptions().entrySet();
			Map.Entry<OWLParser, OWLParserException> ttlParser = (new ArrayList<Map.Entry<OWLParser, OWLParserException>>(
					mapSet)).get(12);
			String ttlError = ttlParser.getValue().toString();
			reporter.addParserMessage(ttlError);
			Exception parserException = new Exception("Parsing process failed.");
			throw parserException;
		}

		loadedOntologyPath = mainOntology.toString();

		String logOntoName;
		if (!ontology.isAnonymous()) {
			logOntoName = ontology.getOntologyID().getOntologyIRI().get().toString();
		} else {
			logOntoName = mainOntology.toString();
			// logger.info("Ontology IRI is anonymous. Use loaded URI/IRI instead.");
		}
		// logger.info("Ontologies loaded! Main Ontology: " + logOntoName);
		System.out.println(logOntoName);
	}

	// just debug method
	public void createJSONModel() {
		System.out.println("We want to create the JSON Model here ");
		DRJsonModel model = new DRJsonModel(manager, ontology);
		model.showModel();

	}

	public void createJSONModelAndWriteIt(String fileName) {
		System.out.println("We want to create the JSON Model here ");
		DRJsonModel model = new DRJsonModel(manager, ontology);
		
		String outputName = "default.json";
		if (fileName != null) {
			outputName = fileName;
		}
		model.writeToFile(outputName);

		
	}

}
