# DR-OWL-API

CLI for backend ontology processing using the OWL-API

# PRELIMINARIES

- maven
- java (using version 11 right now)


# BUILDING THE JAR FILE

On linux `./build_standalone.sh`, this will build the jar file 'owlapi-cli.jar'

On Windows build_standalone_win.bat

# RUNNIG THE JAR FILE

`java -jar owlapi-cli.jar <PARAMS>`


usage: java -jar owlapi-cli.jar

 -dependencies <PATHS>   paths to dependencies of a local ontology

 -file <PATH>            the local path to an ontology

 -h                      views this help text

 -output <PATH>          specify the path for the desired output location

 -task <task>            task specification for CLI



# EXAMPLE FOR ONTOLOGY VALIDATION 

`java -jar owlapi-cli.jar -file <ONTOLOGY FILE PATH> -task validate -output validation_result.txt`

The output is a txt file in a json format that could look something like this : 

> {

>     "imports":"successful",
means that all imported ontologies are successfuly loaded
If not the value will be failed and an additional property (importErrors) will be added describing which imports failed
>     "parser":"successful",
means that inital parsing was fine, otherwise it will add a property (parserErrors) describing <b>currently only</b> ttl parser importErrors

>     "validationErrors":[],
Validation errors will be stored here when the validation flag is equal to failed. 
The errors produce some html code (it is a reuse of the functionality here: https://github.com/protegeproject/protege-owlapi/blob/master/src/main/java/org/protege/owlapi/rdf/Validator.java)
>     "validation":"failed"
> }



