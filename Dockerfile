FROM maven:3.8-jdk-8-slim AS build

WORKDIR = /cli-jar

#PREPARE
RUN apt-get update 
RUN apt-get install -y --no-install-recommends git ca-certificates 



RUN git clone https://gitlab.com/TIBHannover/sc3-project/dr-owl-api.git ./
RUN ./build_standalone.sh

RUN cp owlapi-cli.jar /owlapi-cli.jar


#GET DATA 







