#!/bin/bash

docker build -t testing-jar-builder . && 
docker run --name building_container testing-jar-builder 
docker cp building_container:/owlapi-cli.jar ./
docker rm building_container

# added dockerized builder for owl2vowl
docker build -t owl2vowl-jar-builder owl2Vowl/ 
docker run --name building_container_owl2vowl owl2vowl-jar-builder
docker cp building_container_owl2vowl:/owl2vowl.jar ./
docker rm building_container_owl2vowl

#now we could also remove the images 
#TODO